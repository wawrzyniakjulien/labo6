const api = {
	getLocation: function getLocation(resolve, reject) {

		const options = {
			enableHighAccuracy: true,
			timeout:            5000,
			maximumAge:         0
		};

		function success(pos) {
			const crd = pos.coords;

			fetch(`http://api.wunderground.com/api/a6d7a7067048c586/geolookup/q/${crd.latitude},${crd.longitude}.json`,
				{ methods: "get" })
				.then(function(response) {
					return response.json();
				})
				.then((response) => {
					resolve({
						location: `${response.location.city}, ${response.location.state}`,
						crd:      {
							latitude:  crd.latitude,
							longitude: crd.longitude
						}
					});
				})
				.catch(function(error) {
					console.log("Request failed", error);
					resolve({
						location: `Error`,
						crd:      {
							latitude:  1,
							longitude: 1
						}
					});
				});
		}

		function error(err) {
			console.log(`ERROR(${err.code}): ${err.message}`);
		}

		navigator.geolocation.getCurrentPosition(success, error, options);
	},

	getForecast: function getForecast(lat, long, resolve, reject) {
		fetch(`http://api.wunderground.com/api/a6d7a7067048c586/forecast10day/q/46.786965,-71.293369.json`)
			.then(response => {
				return response.json();
			})
			.then(response => {
				resolve(response.forecast.simpleforecast.forecastday.splice(0, 7));
			})
			.catch(error => {
				console.log("Request failed", error);
				resolve([]);
			});
	}
};

export default api;